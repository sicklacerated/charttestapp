import java.util.Properties

plugins {
    id("com.android.application")
    id("org.jetbrains.kotlin.android")
}

val file = rootProject.file("key.properties")
val properties = Properties().apply {
    load(file.reader())
}

val chartApiKey = properties["chartApiKey"].toString()

android {
    namespace = "com.example.chartapp"
    compileSdk = 34

    defaultConfig {
        applicationId = "com.example.chartapp"
        minSdk = 21
        targetSdk = 34
        versionCode = 1
        versionName = "1.0"

        buildConfigField(type = "String", name = "CHART_API_KEY", value = "\"$chartApiKey\"")

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        vectorDrawables {
            useSupportLibrary = true
        }
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    buildFeatures {
        buildConfig = true
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
    packaging {
        resources {
            excludes += "/META-INF/{AL2.0,LGPL2.1}"
        }
    }
}

val chartVersion = "4.5.0.4839"

dependencies {
    implementation(
        group = "com.scichart.library",
        name = "core",
        version = chartVersion,
        ext = "aar"
    )
    implementation(
        group = "com.scichart.library",
        name = "drawing",
        version = chartVersion,
        ext = "aar"
    )
    implementation(
        group = "com.scichart.library",
        name = "data",
        version = chartVersion,
        ext = "aar"
    )
    implementation(
        group = "com.scichart.library",
        name = "charting",
        version = chartVersion,
        ext = "aar"
    )
    implementation(
        group = "com.scichart.library",
        name = "extensions",
        version = chartVersion,
        ext = "aar"
    )
    implementation("androidx.core:core-ktx:1.12.0")
    implementation("androidx.lifecycle:lifecycle-runtime-ktx:2.6.2")
    implementation("androidx.legacy:legacy-support-v4:1.0.0")
    implementation("androidx.lifecycle:lifecycle-livedata-ktx:2.6.2")
    implementation("androidx.lifecycle:lifecycle-viewmodel-ktx:2.6.2")
    implementation("androidx.appcompat:appcompat:1.6.1")
    implementation("androidx.fragment:fragment-ktx:1.6.1")
    implementation("com.google.android.material:material:1.9.0")
    implementation("androidx.constraintlayout:constraintlayout:2.1.4")
    testImplementation("junit:junit:4.13.2")
    testImplementation("org.jetbrains.kotlinx:kotlinx-coroutines-test:1.7.3")
    androidTestImplementation("androidx.test.ext:junit:1.1.5")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.5.1")
}