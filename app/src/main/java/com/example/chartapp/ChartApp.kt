package com.example.chartapp

import android.app.Application
import com.example.chartapp.di.ChartAppInjector
import com.scichart.charting.visuals.SciChartSurface

class ChartApp : Application() {

    val injector = ChartAppInjector

    override fun onCreate() {
        super.onCreate()

        SciChartSurface.setRuntimeLicenseKey(BuildConfig.CHART_API_KEY)
    }
}