package com.example.chartapp.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProvider.AndroidViewModelFactory.Companion.APPLICATION_KEY
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.CreationExtras
import com.example.chartapp.ChartApp
import com.example.chartapp.domain.repo.ChartDataRepository
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

class ChartAppViewModel(private val chartDataRepository: ChartDataRepository) : ViewModel() {
    private var updateJob: Job? = null

    val controller = ChartController()

    fun startUpdates() {
        if (updateJob?.isActive == true) return

        updateJob = chartDataRepository
            .getChartDataFlow(ChartController.UPDATE_INTERVAL_MS)
            .catch { e -> handleError(e) }
            .onEach(controller::appendData)
            .launchIn(viewModelScope)
    }

    private fun handleError(throwable: Throwable) {
        // TODO Some error handling
        println(throwable)
    }

    override fun onCleared() {
        updateJob?.cancel()
        updateJob = null
        viewModelScope.cancel()
        super.onCleared()
    }

    companion object {
        val factory = object : ViewModelProvider.Factory {
            @Suppress("UNCHECKED_CAST")
            override fun <T : ViewModel> create(modelClass: Class<T>, extras: CreationExtras): T {
                val app = extras[APPLICATION_KEY] as ChartApp
                val repo = app.injector.chartDataRepository
                return ChartAppViewModel(repo) as T
            }
        }
    }
}