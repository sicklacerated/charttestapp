package com.example.chartapp.ui

import android.view.Gravity
import com.example.chartapp.R
import com.example.chartapp.domain.ChartData
import com.scichart.charting.ClipMode
import com.scichart.charting.Direction2D
import com.scichart.charting.model.dataSeries.XyDataSeries
import com.scichart.charting.modifiers.AxisDragModifierBase
import com.scichart.charting.modifiers.PinchZoomModifier
import com.scichart.charting.modifiers.XAxisDragModifier
import com.scichart.charting.modifiers.ZoomPanModifier
import com.scichart.charting.visuals.SciChartSurface
import com.scichart.charting.visuals.annotations.AnnotationLabel
import com.scichart.charting.visuals.annotations.HorizontalLineAnnotation
import com.scichart.charting.visuals.annotations.LabelPlacement
import com.scichart.charting.visuals.axes.AutoRange
import com.scichart.charting.visuals.axes.DateAxis
import com.scichart.charting.visuals.axes.NumericAxis
import com.scichart.charting.visuals.renderableSeries.FastLineRenderableSeries
import com.scichart.core.framework.UpdateSuspender
import com.scichart.data.model.DateRange
import com.scichart.data.model.DoubleRange
import java.util.Date


class ChartController {
    private val dataSeries = XyDataSeries(Date::class.javaObjectType, Int::class.javaObjectType)

    private val lineSeries = FastLineRenderableSeries().also {
        it.dataSeries = dataSeries
    }

    private val dateVisibleRange = DateRange().also {
        val nowMillis = System.currentTimeMillis()
        val maxMillis = nowMillis + MAX_ITEMS_ON_SCREEN * UPDATE_INTERVAL_MS
        it.setMinMax(Date(nowMillis), Date(maxMillis))
    }

    private val zoomXModifier = PinchZoomModifier().also {
        it.direction = Direction2D.XDirection
    }

    private val dragXModifier = XAxisDragModifier().also {
        it.clipModeX = ClipMode.None
        it.dragMode = AxisDragModifierBase.AxisDragMode.Pan
    }

    private val zoomPanModifier = ZoomPanModifier().also {
        it.receiveHandledEvents = true
    }

    private val modifiers = listOf(zoomXModifier, dragXModifier, zoomPanModifier)

    private var surface: SciChartSurface? = null
    private var lineAnnotation: HorizontalLineAnnotation? = null
    private var lineAnnotationLabel: AnnotationLabel? = null

    fun setSurface(surface: SciChartSurface) {
        this.surface = surface

        performUpdates {
            val xAxis = DateAxis(context).also {
                it.autoRange = AutoRange.Never
                it.visibleRange = dateVisibleRange
                it.axisTitle = context.getString(R.string.date_axis_title)
            }

            val yAxis = NumericAxis(context).also {
                it.autoRange = AutoRange.Always
                it.growBy = DoubleRange(0.1, 0.1)
                it.axisTitle = context.getString(R.string.value_axis_title)
            }

            xAxes.add(xAxis)
            yAxes.add(yAxis)

            renderableSeries.add(lineSeries)
            chartModifiers.addAll(modifiers)

            val annotationLabel = AnnotationLabel(context).also {
                it.labelPlacement = LabelPlacement.Axis
                lineAnnotationLabel = it
            }

            val annotation = HorizontalLineAnnotation(context).also {
                it.setIsEditable(true)
                it.annotationLabels.add(annotationLabel)
                lineAnnotation = it
            }

            annotations.add(annotation)
        }
    }

    fun appendData(chartData: ChartData) {
        performUpdates {
            dataSeries.append(chartData.timestamp, chartData.value)
            updateAnnotation(chartData)
            zoomExtentsY()
        }
    }

    private fun updateAnnotation(chartData: ChartData) {
        val annotation = lineAnnotation ?: return
        val annotationLabel = lineAnnotationLabel ?: return

        annotationLabel.text = chartData.value.toString()

        annotation.also {
            it.x1 = chartData.timestamp
            it.y1 = chartData.value
            it.horizontalGravity = Gravity.END
        }
    }

    private inline fun performUpdates(crossinline block: SciChartSurface.() -> Unit) {
        surface?.let {
            UpdateSuspender.using(it) {
                it.block()
            }
        }
    }

    fun resetSurface() {
        lineAnnotation = null
        lineAnnotationLabel = null
        surface = null
    }

    companion object {
        private const val MAX_ITEMS_ON_SCREEN = 50
        const val UPDATE_INTERVAL_MS = 1000L
    }
}