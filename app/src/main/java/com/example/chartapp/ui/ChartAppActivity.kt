package com.example.chartapp.ui

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.example.chartapp.R
import com.scichart.charting.visuals.SciChartSurface

class ChartAppActivity : AppCompatActivity() {
    private val viewModel: ChartAppViewModel by viewModels { ChartAppViewModel.factory }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chart_app)

        val surface = findViewById<SciChartSurface>(R.id.chart_surface)
        viewModel.controller.setSurface(surface)
        viewModel.startUpdates()
    }

    override fun onDestroy() {
        viewModel.controller.resetSurface()
        super.onDestroy()
    }
}

