package com.example.chartapp.data.repo

import com.example.chartapp.domain.ChartData
import com.example.chartapp.domain.repo.ChartDataRepository
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import java.util.Date

class RandomChartDataRepository(
    private val randomValueSupplier: () -> Int,
    private val timeSupplier: () -> Long,
) : ChartDataRepository {

    override fun getChartDataFlow(intervalMs: Long): Flow<ChartData> {
        return flow {
            var lastChartData: ChartData? = null

            while (true) {
                val chartData = getChartData(lastChartData)
                lastChartData = chartData
                emit(chartData)
                delay(intervalMs)
            }
        }
    }

    private fun getChartData(previous: ChartData?): ChartData {
        val randomValue = randomValueSupplier()

        val value = if (previous != null) {
            previous.value + randomValue
        } else {
            randomValue
        }

        return ChartData(timestamp = Date(timeSupplier()), value = value)
    }
}
