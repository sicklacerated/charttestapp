package com.example.chartapp.di

import com.example.chartapp.data.repo.RandomChartDataRepository
import com.example.chartapp.domain.repo.ChartDataRepository
import kotlin.random.Random
import kotlin.random.nextInt

object ChartAppInjector {
    val chartDataRepository: ChartDataRepository by lazy {
        RandomChartDataRepository(
            randomValueSupplier = { Random.nextInt(IntRange(-1, 1)) },
            timeSupplier = { System.currentTimeMillis() },
        )
    }
}