package com.example.chartapp.domain.repo

import com.example.chartapp.domain.ChartData
import kotlinx.coroutines.flow.Flow

interface ChartDataRepository {
    fun getChartDataFlow(intervalMs: Long): Flow<ChartData>
}