package com.example.chartapp.domain

import java.util.Date

data class ChartData(val timestamp: Date, val value: Int)