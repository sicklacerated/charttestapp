package com.example.chartapp.data

import com.example.chartapp.data.repo.RandomChartDataRepository
import com.example.chartapp.domain.ChartData
import kotlinx.coroutines.cancelAndJoin
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Assert.assertThrows
import org.junit.Test
import java.io.IOException
import java.util.*

class RandomChartDataRepositoryTest {
    @Test
    fun shouldEmitChartDataUntilCancelled() = runTest {
        val repo = RandomChartDataRepository(
            randomValueSupplier = { 1 },
            timeSupplier = { 1000L },
        )

        val chartDataList = mutableListOf<ChartData>()

        val job = launch {
            repo.getChartDataFlow(100).toList(chartDataList)
        }

        delay(1000)
        job.cancelAndJoin()

        assertEquals(chartDataList.count(), 10)
    }

    @Test
    fun shouldEmitValidChartData() = runTest {
        val mockRandomValue = 1
        val mockTime = 1000L

        val repo = RandomChartDataRepository(
            randomValueSupplier = { mockRandomValue },
            timeSupplier = { mockTime },
        )

        val chartDataList = mutableListOf<ChartData>()

        val expectedStartData = ChartData(timestamp = Date(mockTime), value = mockRandomValue)
        val expectedNextData = ChartData(timestamp = Date(mockTime), value = mockRandomValue + mockRandomValue)

        val job = launch {
            repo.getChartDataFlow(100).toList(chartDataList)
        }

        delay(200)
        job.cancelAndJoin()

        assertEquals(chartDataList.count(), 2)

        val startData = chartDataList.first()
        assertEquals(expectedStartData, startData)

        val nextData = chartDataList.last()
        assertEquals(expectedNextData, nextData)
    }

    @Test
    fun shouldThrowIfErrorOccurred() {
        val repo = RandomChartDataRepository(
            randomValueSupplier = { throw IOException() },
            timeSupplier = { 1000L },
        )

        assertThrows(IOException::class.java) {
            runTest {
                val job = launch {
                    repo.getChartDataFlow(100).toList()
                }

                delay(200)
                job.cancelAndJoin()
            }
        }
    }
}